FROM golang:1.17 AS builder
WORKDIR /workspace

# Get the dependencies so it can be cached into a layer
COPY go.mod go.sum ./
RUN go mod download

# Now copy all the source
COPY . .

ARG revisionID=unknown
ARG buildTimestamp=unknwon

# ...and build it.
RUN CGO_ENABLED=0 go build -o ./bin/alpha-rest \
  -ldflags="-s -w -X main.revisionID=${revisionID} -X main.buildTimestamp=${buildTimestamp} -extldflags \"-static\"" \
  .

# build the runtime image
FROM alpine:3.11
WORKDIR /root/
RUN apk add --no-cache --virtual .build-deps \
  ca-certificates \
  && update-ca-certificates \
  # Clean up when done
  && rm -rf /tmp/* \
  && apk del .build-deps

# Include resources required by Ladies
COPY ./resources ./resources

COPY --from=builder /workspace/bin/alpha-rest ./service

EXPOSE 8080
ENTRYPOINT ["./service"]