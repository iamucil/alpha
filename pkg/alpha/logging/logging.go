package logging

import (
	"time"

	"github.com/getsentry/sentry-go"
	"github.com/rs/zerolog"
	"github.com/timemore/foundation/logger"
)

// func init() {
// 	_ = sentry.Init(sentry.ClientOptions{
// 		Dsn: "https://0b1da1c9d24049e2ae313a216e578263@o415299.ingest.sentry.io/5908432",
// 	})
// }

type RavenHook struct{}

func (h RavenHook) Run(e *zerolog.Event, level zerolog.Level, msg string) {
	if level == zerolog.ErrorLevel || level == zerolog.WarnLevel {
		go func() {
			sentry.ConfigureScope(func(scope *sentry.Scope) {
				scope.SetTag("secretTag", "go#1")
				scope.SetContext("character", map[string]interface{}{
					"name":        "Mighty Fighter",
					"age":         19,
					"attack_type": "melee",
				})
			})

			defer sentry.Flush(2 * time.Second)

			e.Str("sending to sentry", "true")
			sentry.CaptureMessage(msg)
		}()
	}
}

// NewPkgLogger creates a logger for use within a package. This logger
// automatically adds the name of the package where this function was called,
// not when logging.
func NewPkgLogger() Logger {
	pkgLogger := logger.NewPkgLogger()
	// stdLog := pkgLogger.Hook(RavenHook{})
	// pkgLogger.Logger = stdLog
	return Logger{PkgLogger: pkgLogger}
}

// Logger is a specialized logger for logging with ALPHA-specific context
type Logger struct {
	logger.PkgLogger
}
