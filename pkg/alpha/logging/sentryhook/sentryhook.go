package sentryhook

import (
	"github.com/getsentry/sentry-go"
)

type SentryHook struct {
}

func init() {
	_ = sentry.Init(sentry.ClientOptions{
		Dsn: "https://0b1da1c9d24049e2ae313a216e578263@o415299.ingest.sentry.io/5908432",
	})
}
