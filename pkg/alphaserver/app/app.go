package app

import (
	"github.com/rez-go/stev"
	"github.com/timemore/foundation/app"
	"github.com/timemore/foundation/errors"
	"timemore.internal/alpha/pkg/alpha/logging"
	"timemore.internal/alpha/pkg/alphaserver"
)

var log = logging.NewPkgLogger()

type App struct {
	app.App

	Core *alphaserver.Core
}

func NewByEnv(envPrefix string) (*App, error) {
	cfg := ConfigSkeleton()
	err := stev.LoadEnv(envPrefix, &cfg)
	if err != nil {
		err = errors.Wrap("config loading from environment variables", err)
	}

	cfg.EnvPrefix = envPrefix
	return newApp(cfg)
}

func newApp(cfg Config) (*App, error) {
	appCore, err := app.Init(cfg.AppInfo)
	if err != nil {
		return nil, errors.Wrap("app initialization", err)
	}

	srvCore, err := alphaserver.NewCoreByConfig(cfg.Core, appCore)
	if err != nil {
		log.Fatal().Err(err).Msgf("core initialization")
	}
	return &App{
		App:  appCore,
		Core: srvCore,
	}, nil
}
