package app

import (
	"github.com/timemore/foundation/app"
	"timemore.internal/alpha/pkg/alphaserver"
)

type Config struct {
	EnvPrefix string
	AppInfo   *app.Info              `env:"APP"`
	Core      alphaserver.CoreConfig `env:",squash"`
}

func ConfigSkeleton() Config {
	return Config{
		Core: alphaserver.CoreConfigSkeleton(),
	}
}
