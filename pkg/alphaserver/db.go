package alphaserver

import (
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/timemore/foundation/errors"
)

func connectToPostgres(dbURL string) (*sqlx.DB, error) {
	var db *sqlx.DB
	parsedURL, err := url.Parse(dbURL)
	if err != nil {
		return nil, err
	}

	var maxIdleConns, maxOpenConns int64
	queryPart := parsedURL.Query()
	if maxIdleConnsStr := queryPart.Get("max_idle_conns"); maxIdleConnsStr != "" {
		queryPart.Del("max_idle_conns")
		maxIdleConns, err = strconv.ParseInt(maxIdleConnsStr, 10, 32)
		if err != nil {
			return nil, errors.Wrap("unable to parse max_idle_conns query parameter", err)
		}
	}

	if maxOpenConnsStr := queryPart.Get("max_open_conns"); maxOpenConnsStr != "" {
		queryPart.Del("max_open_conns")
		maxOpenConns, err = strconv.ParseInt(maxOpenConnsStr, 10, 32)
		if err != nil {
			return nil, errors.Wrap("unable to parse max_open_conns query parameter", err)
		}
	}

	if maxIdleConns == 0 {
		maxIdleConns = 2
	}
	if maxOpenConns == 0 {
		maxOpenConns = 8
	}
	parsedURL.RawQuery = queryPart.Encode()
	dbURL = parsedURL.String()
	for {
		db, err = sqlx.Connect("postgres", dbURL)
		if err == nil {
			break
		}
		if !strings.Contains(err.Error(), "connect: connection refused") {
			return nil, err
		}

		const retryDuration = 5 * time.Second
		time.Sleep(retryDuration)
	}
	// Pool of connections
	if db != nil {
		db.SetMaxIdleConns(int(maxIdleConns))
		db.SetMaxOpenConns(int(maxOpenConns))
	}

	return db, nil
}

func doTx(db *sqlx.DB, txFunc func(tx *sqlx.Tx) error) error {
	tx, err := db.Beginx()
	if err != nil {
		return err
	}
	defer func() {
		if rec := recover(); rec != nil {
			tx.Rollback()
			panic(rec)
		} else if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()

	err = txFunc(tx)
	return err
}
