package alphaserver

import (
	"net/http"
)

type RESTServiceServerBase struct {
	*Core
}

func RESTServiceServerWith(
	alphaServerCore *Core) *RESTServiceServerBase {
	if alphaServerCore == nil {
		panic("provided alphaServerCore is nil")
	}
	return &RESTServiceServerBase{alphaServerCore}
}

// RequestHasNoAuthorization checks if the request header with key
// Authorization has non empty value. This DOES NOT check if it's valid
// or not.
func (svcBase *RESTServiceServerBase) RequestHasNoAuthorization(req *http.Request) bool {
	if req == nil {
		return true
	}

	authHeaderVal := req.Header.Get("Authorization")
	return authHeaderVal == ""
}
