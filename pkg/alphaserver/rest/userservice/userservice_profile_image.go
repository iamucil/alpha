package userservice

import (
	"net/http"

	"github.com/emicklei/go-restful/v3"
	"github.com/timemore/foundation/api/rest"
)

const multipartFormMaxMemory = 20 * 1024 * 1024

func (svc *UserService) putUserProfileImage(req *restful.Request, resp *restful.Response) {
	if err := req.Request.ParseMultipartForm(multipartFormMaxMemory); err != nil {
		logReq(req.Request).Warn().Err(err).Msgf("unable to parse form data")
		rest.RespondTo(resp).Error(rest.ErrorResponse{
			Description: "Parse multipart form data",
		}, http.StatusBadRequest)
		return
	}

	uploadedFile, _, err := req.Request.FormFile("body")
	if err != nil {
		logReq(req.Request).Warn().Err(err).Msgf("invalid image file")
		rest.RespondTo(resp).Error(rest.ErrorResponse{
			Description: "invalid image file",
			Fields: []rest.ErrorResponseField{
				{Field: "form.body", Description: err.Error()},
			},
		}, http.StatusBadRequest)
		return
	}
	defer func() {
		_ = uploadedFile.Close()
	}()

	profileImageURL, err := svc.restSrvBase.SetUserProfileImage(uploadedFile)
	if err != nil {
		log.Warn().Err(err).Msgf("unable to set profile image")
		rest.RespondTo(resp).Error(rest.ErrorResponse{
			Description: "unable to set profile image",
			Fields: []rest.ErrorResponseField{
				{Field: "form.Body", Description: err.Error()},
			},
		}, http.StatusBadRequest)
		return
	}

	rest.RespondTo(resp).Success(userProfileImagePutResponse{
		URL: profileImageURL,
	})
}

func (svc *UserService) getUserProfileImage(req *restful.Request, resp *restful.Response) {}
