package userservice

import (
	"net/http"
	"strings"

	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/timemore/foundation/api/rest"
	"timemore.internal/alpha/pkg/alpha/logging"
	"timemore.internal/alpha/pkg/alphaserver"
)

var (
	log    = logging.NewPkgLogger()
	logReq = log.WithRequest
)

type UserService struct {
	basePath    string
	restSrvBase *alphaserver.RESTServiceServerBase
}

func New(basePath string, restSvcBase *alphaserver.RESTServiceServerBase) *UserService {
	return &UserService{
		basePath:    basePath,
		restSrvBase: restSvcBase,
	}
}

type userProfileImagePutResponse struct {
	URL string `json:"url"`
}

func (svc *UserService) RestfulWebService() *restful.WebService {
	restWS := new(restful.WebService)
	restWS.Path(svc.basePath).
		Produces(restful.MIME_JSON).
		Consumes(restful.MIME_JSON)

	tags := []string{"api.v1.users"}
	restWS.Route(restWS.GET("/{user-id}").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Doc("Obtain user personal info").
		To(svc.obtainUserInfo))

	restWS.Route(restWS.PUT("/me/profile_image").
		Consumes("multipart/form-data").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Doc("put profile image for user").
		Param(restWS.
			FormParameter(
				"body", "File to upload").
			DataType("file").
			Required(true)).
		To(svc.putUserProfileImage).
		Returns(http.StatusInternalServerError, "An unexpected condition was encountered in processing the request", nil).
		Returns(http.StatusBadRequest, "The server cannot or will not process the request due to an apparent client error", nil).
		Returns(http.StatusUnauthorized, "Authentication is required and has failed or has not yet been provided", nil).
		Returns(http.StatusNotAcceptable, "The target resource does not have a current representation that would be acceptable.", nil).
		Returns(http.StatusOK, "Profile image updated", userProfileImagePutResponse{}))

	restWS.Route(restWS.GET("/me/profile_image").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Doc("get profile image").
		To(svc.getUserProfileImage).
		Returns(http.StatusInternalServerError, "An unexpected condition was encountered in processing the request", nil).
		Returns(http.StatusBadRequest, "The server cannot or will not process the request due to an apparent client error", nil).
		Returns(http.StatusUnauthorized, "Authentication is required and has failed or has not yet been provided", nil).
		Returns(http.StatusNotAcceptable, "The target resource does not have a current representation that would be acceptable.", nil).
		Returns(http.StatusOK, "OK", nil))

	return restWS
}

func (svc *UserService) obtainUserInfo(req *restful.Request, resp *restful.Response) {
	userIDStr := req.PathParameter("user-id")
	if strings.ToLower(userIDStr) == "me" {
		rest.RespondTo(resp).Success(map[string]interface{}{"user": userIDStr})
		return
	}
	rest.RespondTo(resp).Success(nil)
}
