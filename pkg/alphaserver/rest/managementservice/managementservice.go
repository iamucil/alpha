package managementservice

import (
	"time"

	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/getsentry/sentry-go"
	"github.com/timemore/foundation/api/rest"
	"github.com/timemore/foundation/errors"
	"timemore.internal/alpha/pkg/alpha/logging"
	"timemore.internal/alpha/pkg/alphaserver"
)

var (
	log    = logging.NewPkgLogger()
	logReq = log.WithRequest
)

type ManagementService struct {
	basePath    string
	restSrvBase *alphaserver.RESTServiceServerBase
}

func New(basePath string, restSvcBase *alphaserver.RESTServiceServerBase) *ManagementService {
	return &ManagementService{
		basePath:    basePath,
		restSrvBase: restSvcBase,
	}
}

type userProfileImagePutResponse struct {
	URL string `json:"url"`
}

func (svc *ManagementService) RestfulWebService() *restful.WebService {
	restWS := new(restful.WebService)
	restWS.Path(svc.basePath).
		Produces(restful.MIME_JSON).
		Consumes(restful.MIME_JSON)

	tags := []string{"api.v1.manage"}
	restWS.Route(restWS.GET("/ping").
		Metadata(restfulspec.KeyOpenAPITags, tags).
		Doc("Obtain user personal info").
		To(svc.ping))

	return restWS
}

func (svc *ManagementService) ping(req *restful.Request, resp *restful.Response) {
	tNow := time.Now().UTC()
	hub := sentry.GetHubFromContext(req.Request.Context())
	hub.CaptureException(errors.ArgMsg("sentry", tNow.String()))
	rest.RespondTo(resp).Success("OK")
}
