package rest

import (
	"github.com/emicklei/go-restful/v3"
	"timemore.internal/alpha/pkg/alpha/logging"
	"timemore.internal/alpha/pkg/alphaserver"
	"timemore.internal/alpha/pkg/alphaserver/rest/managementservice"
	"timemore.internal/alpha/pkg/alphaserver/rest/userservice"
)

const ServiceLatestVersionString = "v1"

var log = logging.NewPkgLogger()

// InitRESTV1Services load all rest-services here
func InitRESTV1Services(servePath string, container *restful.Container, srvCore *alphaserver.Core) {
	restServerBase := alphaserver.RESTServiceServerWith(srvCore)
	log.Info().Msgf("Initializing rest services")
	userSvc := userservice.New(servePath+"/users", restServerBase)
	container.Add(userSvc.RestfulWebService())

	managementSvc := managementservice.New(servePath+"/manage", restServerBase)
	log.Info().Msgf("Initializing management services")
	container.Add(managementSvc.RestfulWebService())
}
