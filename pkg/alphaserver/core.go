package alphaserver

import (
	"fmt"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/timemore/foundation/app"
	"github.com/timemore/foundation/errors"
	mediastore "github.com/timemore/foundation/media/store"

	// 	Media object storage modules
	_ "github.com/timemore/foundation/media/store/minio"
)

const secretFilesDir = "/run/secrets"

type Core struct {
	realmName string
	db        *sqlx.DB

	mediaStore *mediastore.Store
}

type CoreConfig struct {
	RealmName                string            `env:"REALM_NAME"`
	DBUrl                    string            `env:"DB_URL,required"`
	DBInstanceConnectionName string            `env:"DB_INSTANCE_CONNECTION_NAME"`
	DBSocketDirectory        string            `env:"DB_SOCKET_DIRECTORY"`
	Media                    mediastore.Config `env:"MEDIA"`
}

func CoreConfigSkeleton() CoreConfig {
	return CoreConfig{
		Media: mediastore.ConfigSkeleton(),
	}
}

func NewCoreByConfig(cfg CoreConfig, appApp app.App) (*Core, error) {
	appInfo := appApp.AppInfo()
	appName := appInfo.Name

	realmName := cfg.RealmName
	if realmName == "" {
		realmName = appName
	}

	instance := &Core{
		realmName: realmName,
	}

	var instanceConnectionName string
	var socketDir string
	if cfg.DBInstanceConnectionName != "" {
		instanceConnectionName = cfg.DBInstanceConnectionName
		socketDir = "/cloudsql"
		if cfg.DBSocketDirectory != "" {
			socketDir = cfg.DBSocketDirectory
		}
	}
	var coreDB *sqlx.DB
	var err error
	coreDBUrl := cfg.DBUrl
	if coreDBUrl != "" {
		parts := strings.SplitN(coreDBUrl, ":", 2)
		if len(parts) != 2 {
			return nil, errors.New("core DB connection string is invalid")
		}
		switch parts[0] {
		case "postgres", "postgresql", "pg":
			coreDB, err = connectToPostgreSQL(cfg.DBUrl, instanceConnectionName, socketDir)
			if err != nil {
				return nil, errors.Wrap("DB Connection", err)
			}

		default:
			return nil, errors.New("unsupported database driver")
		}
	}
	instance.db = coreDB
	log.Info().Msg("Initializing media service...")
	mediaStoreModules := mediastore.ModuleNames()
	log.Info().Msgf("Registered media object storage service integrations: %v",
		mediaStoreModules)

	mediaStore, err := mediastore.New(cfg.Media)
	if err != nil {
		return nil, errors.Wrap("media store initialization", err)
	}
	instance.mediaStore = mediaStore
	return instance, nil
}

func connectToPostgreSQL(dbURL string,
	socketDir, instanceConnName string) (*sqlx.DB, error) {
	var db *sqlx.DB
	parsedURL, err := url.Parse(dbURL)
	if err != nil {
		return nil, err
	}

	var maxIdleConns, maxOpenConns int64
	queryPart := parsedURL.Query()
	if maxIdleConnsStr := queryPart.Get("max_idle_conns"); maxIdleConnsStr != "" {
		queryPart.Del("max_idle_conns")
		maxIdleConns, err = strconv.ParseInt(maxIdleConnsStr, 10, 32)
		if err != nil {
			return nil, errors.Wrap("Unable to parse max_idle_conns query parameter", err)
		}
	}

	if maxOpenConnsStr := queryPart.Get("max_open_conns"); maxOpenConnsStr != "" {
		queryPart.Del("max_open_conns")
		maxOpenConns, err = strconv.ParseInt(maxOpenConnsStr, 10, 32)

		if err != nil {
			return nil, errors.Wrap("Unable to parse max_open_conns query parameter", err)
		}
	}

	if maxOpenConns == 0 {
		maxOpenConns = 16
	}
	if maxIdleConns == 0 {
		maxIdleConns = 4
	}

	parsedURL.RawQuery = queryPart.Encode()
	dbURL = parsedURL.String()
	if instanceConnName != "" {
		userInfo := parsedURL.User
		dbUser := userInfo.Username()
		dbPass, _ := userInfo.Password()
		dbName := strings.TrimLeft(parsedURL.Path, "/")
		dbURL = cloudSqlDBUrl(socketDir, instanceConnName, dbName, dbUser, dbPass)
	}

	for {
		db, err = sqlx.Connect("postgres", dbURL)
		if err == nil {
			break
		}

		if !strings.Contains(err.Error(), "connect: connection refused") || !strings.Contains(err.Error(), "pq: the database system is starting up") {
			return nil, err
		}

		const retryDuration = 5 * time.Second
		time.Sleep(retryDuration)
	}

	if db != nil {
		db.SetMaxOpenConns(int(maxOpenConns))
		db.SetMaxIdleConns(int(maxIdleConns))
		db.SetConnMaxLifetime(1800)
	}

	return db, nil
}

func cloudSqlDBUrl(socketDir string, instanceConnName string, name string,
	username string, password string) string {
	return fmt.Sprintf("user=%s password=%s database=%s host=%s/%s", username, password, name, socketDir,
		instanceConnName)
}
