package alphaserver

import (
	"io"
	"path/filepath"

	"github.com/minio/minio-go/v7"
	"github.com/timemore/foundation/errors"
	"github.com/timemore/foundation/media"
)

type ProfileImageFile interface {
	io.Reader
	io.Seeker
}

func (core *Core) SetUserProfileImage(imageFile ProfileImageFile) (imageURL string, err error) {
	imageTypeBytes := make([]byte, 512)
	_, err = imageFile.Read(imageTypeBytes)
	_, _ = imageFile.Seek(0, io.SeekStart)

	const bucketProfileImagePath = "/profile_images"
	mediaTypeInfo := media.GetMediaTypeInfo(media.MediaType_IMAGE)
	if mediaTypeInfo == nil {
		return "", errors.Msg("media type info invalid")
	}
	contentType := media.DetectType(imageTypeBytes)
	if !mediaTypeInfo.IsContentTypeAllowed(contentType) {
		return "", errors.ArgMsg("imageFile", "image content type not allowed")
	}

	filename := core.mediaStore.GenerateName(imageFile)
	_, _ = imageFile.Seek(0, io.SeekStart)

	uploadInfo, err := core.mediaStore.Upload(
		filepath.Join(bucketProfileImagePath, filename),
		imageFile,
		mediaTypeInfo.MediaType())
	if err != nil {
		return "", errors.Wrap("set profile image", err)
	}
	uploadObject, ok := uploadInfo.(minio.UploadInfo)
	if !ok {
		return "", errors.EntMsg("upload object", "media type unsupported")
	}
	// pre-signed url will be expired in 1 day
	imageURL, err = core.mediaStore.GetPublicURL(uploadObject.Key)
	return
}
