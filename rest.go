package main

import (
	"bytes"
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"

	"github.com/emicklei/go-restful/v3"
	"github.com/getsentry/sentry-go"
	"github.com/go-openapi/spec"
	"github.com/timemore/foundation/app"
)

func processSwaggerSpec(swaggerSpec *spec.Swagger, appInfo app.Info, secDefs spec.SecurityDefinitions) {
	buildInfo := app.GetBuildInfo()
	rev := buildInfo.RevisionID
	if rev != "unknown" && len(rev) > 8 {
		rev = rev[:8]
	}
	swaggerSpec.Info = &spec.Info{
		InfoProps: spec.InfoProps{
			Title:       "Alpha",
			Description: "Alpha services",
			Version:     fmt.Sprintf("0.0.0-%s built at %s", rev, buildInfo.Timestamp),
		},
	}

	for k := range swaggerSpec.Paths.Paths {
		swaggerSpec.Paths.Paths[k] = processOpenAPIPath(swaggerSpec.Paths.Paths[k], secDefs)
	}
	swaggerSpec.SecurityDefinitions = secDefs
}

func processOpenAPIPath(pathItem spec.PathItem, secDefs spec.SecurityDefinitions) spec.PathItem {
	pathItem.Get = processOpenAPIPathOp(pathItem.Get, secDefs)
	pathItem.Put = processOpenAPIPathOp(pathItem.Put, secDefs)
	pathItem.Post = processOpenAPIPathOp(pathItem.Post, secDefs)
	pathItem.Delete = processOpenAPIPathOp(pathItem.Delete, secDefs)
	pathItem.Options = processOpenAPIPathOp(pathItem.Options, secDefs)
	pathItem.Head = processOpenAPIPathOp(pathItem.Head, secDefs)
	pathItem.Patch = processOpenAPIPathOp(pathItem.Patch, secDefs)
	return pathItem
}

func processOpenAPIPathOp(op *spec.Operation, secDefs spec.SecurityDefinitions) *spec.Operation {
	if op == nil {
		return nil
	}

	for _, tag := range op.Tags {
		if tag == "hidden" {
			return nil
		}
	}

	var updatedParams []spec.Parameter
	for _, p := range op.Parameters {
		isSec := false
		if p.Description != "" {
			lowerDesc := strings.ToLower(p.Description)
			for k, secDef := range secDefs {
				if strings.HasPrefix(lowerDesc, k) {
					if secDef.Type == "basic" {
						// Basic authorization is always as 'Authorization' in the header
						if p.Name == "Authorization" && p.In == "header" {
							op.Security = append(op.Security, map[string][]string{k: {}})
							isSec = true
							continue
						}
					}
					if secDef.Type == "apiKey" {
						if p.Name == secDef.Name && p.In == secDef.In {
							op.Security = append(op.Security, map[string][]string{k: {}})
							isSec = true
							continue
						}
					}
				}
			}
		}
		if !isSec {
			updatedParams = append(updatedParams, p)
		}
	}
	op.Parameters = updatedParams
	return op
}

func setupSentryFilter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	_ = sentry.Init(sentry.ClientOptions{
		Dsn: "https://0b1da1c9d24049e2ae313a216e578263@o415299.ingest.sentry.io/5908432",
	})
	inBody, err := ioutil.ReadAll(req.Request.Body)
	if err != nil {
		_ = resp.WriteError(400, err)
		return
	}

	c := NewResponseCapture(resp.ResponseWriter)
	resp.ResponseWriter = c

	ctx := req.Request.Context()
	hub := sentry.GetHubFromContext(ctx)
	if hub == nil {
		hub = sentry.CurrentHub().Clone()
		ctx = sentry.SetHubOnContext(ctx, hub)
	}
	span := sentry.StartSpan(ctx, "http.server",
		sentry.TransactionName(fmt.Sprintf("%s %s", req.Request.Method, req.Request.URL.Path)),
		sentry.ContinueFromRequest(req.Request),
	)
	defer span.Finish()
	// TODO(tracing): if the next handler.ServeHTTP panics, store
	// information on the transaction accordingly (status, tag,
	// level?, ...).
	r := req.Request.WithContext(span.Context())
	r.Body = ioutil.NopCloser(bytes.NewReader(inBody))
	hub.Scope().SetRequest(r)
	hub.Scope().SetRequestBody(inBody)

	defer func(hub *sentry.Hub, r *http.Request) {
		if err := recover(); err != nil {
			eventID := hub.RecoverWithContext(
				context.WithValue(r.Context(), sentry.RequestContextKey, r),
				err,
			)
			if eventID != nil {
				hub.Flush(2 * time.Second)
			}
		}
	}(hub, r)
	req.Request = r
	chain.ProcessFilter(req, resp)
	hub.Scope().SetContext("identifier", map[string]interface{}{
		"name":        "Mighty Fighter",
		"age":         19,
		"attack_type": "melee",
	})
	hub.CaptureMessage("hello world: " + r.RemoteAddr + fmt.Sprintf("status: %d", resp.StatusCode()))

}

type ResponseCapture struct {
	http.ResponseWriter
	wroteHeader bool
	status      int
	body        *bytes.Buffer
}

func NewResponseCapture(w http.ResponseWriter) *ResponseCapture {
	return &ResponseCapture{
		ResponseWriter: w,
		wroteHeader:    false,
		body:           new(bytes.Buffer),
	}
}

func (c ResponseCapture) Header() http.Header {
	return c.ResponseWriter.Header()
}

func (c ResponseCapture) Write(data []byte) (int, error) {
	if !c.wroteHeader {
		c.WriteHeader(http.StatusOK)
	}
	c.body.Write(data)
	return c.ResponseWriter.Write(data)
}

func (c *ResponseCapture) WriteHeader(statusCode int) {
	c.status = statusCode
	c.wroteHeader = true
	c.ResponseWriter.WriteHeader(statusCode)
}

func (c ResponseCapture) Bytes() []byte {
	return c.body.Bytes()
}

func (c ResponseCapture) StatusCode() int {
	return c.status
}
