#!/bin/sh -
set -e

RESOLVED_RESOURCE_VERSION=$(git rev-parse HEAD)
BUILD_TIMESTAMP=$(date -u +"%Y-%m-%dT%H:%M:%S%:::z")
NGROK_URL=$(curl -s http://localhost:4040/api/tunnels/command_line | jq -r --unbuffered '.public_url')
if [ -z "${NGROK_URL}" ]; then
  APP_URL=http://localhost
else
  APP_URL=$NGROK_URL
fi

CURRENT_UID=$(id -u ${USER}):$(id -g ${USER}) \
REVISION_ID=${RESOLVED_RESOURCE_VERSION} \
BUILD_TIMESTAMP=${BUILD_TIMESTAMP} \
APP_URL=${APP_URL} \
docker-compose up --build --abort-on-container-exit --remove-orphans
