package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	restfulspec "github.com/emicklei/go-restful-openapi/v2"
	"github.com/emicklei/go-restful/v3"
	"github.com/go-openapi/spec"
	"github.com/timemore/foundation/api/rest"

	"timemore.internal/alpha/pkg/alpha/logging"
	srvapp "timemore.internal/alpha/pkg/alphaserver/app"
	srvrest "timemore.internal/alpha/pkg/alphaserver/rest"
)

var (
	log    = logging.NewPkgLogger()
	logReq = log.WithRequest
)

var (
	revisionID     = "unknown"
	buildTimestamp = "unknown"
)

func main() {
	for _, flag := range os.Args[:] {
		if flag == "--version" {
			return
		}
	}
	srvApp, err := srvapp.NewByEnv("ALPHA_")
	if err != nil {
		log.Fatal().Err(err).Msgf("server app initialization")
	}

	_, _ = fmt.Fprintf(os.Stdout, "%s rest service revision %s\nbuilt at %s\n",
		srvApp.AppInfo().Name,
		revisionID,
		buildTimestamp)
	apiBasePath := "/" + srvrest.ServiceLatestVersionString
	if apiBasePathStr, found := os.LookupEnv("API_BASE_PATH"); found {
		if apiBasePathStr != "" {
			apiBasePath = strings.TrimSuffix(apiBasePathStr, "/")
		}
	}

	httpListenPort := os.Getenv("LISTEN_PORT")
	if httpListenPort == "" {
		httpListenPort = "8080"
	}

	apiDocsPath := apiBasePath + "/apidocs.json"
	apiMux := http.NewServeMux()
	restContainer := restful.NewContainer()
	restContainer.ServeMux = apiMux
	restContainer.EnableContentEncoding(true)

	// add container filter to enable CORS
	_ = rest.SetupCORSFilterByEnv(restContainer, "")
	// 	add container filter to respond to OPTIONS
	restContainer.Filter(restContainer.OPTIONSFilter)
	// 	logger?
	restContainer.Filter(setupSentryFilter)
	srvrest.InitRESTV1Services(apiBasePath, restContainer, srvApp.Core)

	// if not handled by service, all errors with be caught here
	restContainer.ServiceErrorHandler(func(serviceError restful.ServiceError, req *restful.Request, resp *restful.Response) {
		logReq(req.Request).Warn().Int("status_code", serviceError.Code).Str("err_msg", serviceError.Message).Msg("routing error")
		_ = resp.WriteErrorString(serviceError.Code, serviceError.Message)
	})

	secDefs := spec.SecurityDefinitions{
		"basic-oauth2-client-creds": spec.BasicAuth(),
		"bearer-access-token":       spec.APIKeyAuth("Authorization", "header"),
	}
	// Setup API Specification handler
	restContainer.Add(restfulspec.NewOpenAPIService(restfulspec.Config{
		WebServices: restContainer.RegisteredWebServices(),
		APIPath:     apiDocsPath,
		PostBuildSwaggerObjectHandler: func(swaggerSpec *spec.Swagger) {
			processSwaggerSpec(swaggerSpec, srvApp.AppInfo(), secDefs)
		},
	}))

	// 	load documentation with swagger-ui
	apiMux.Handle(apiBasePath+"/apidocs/",
		http.StripPrefix(apiBasePath+"/apidocs/",
			http.FileServer(http.Dir("resources/swagger-ui"))))

	rootMux := http.NewServeMux()
	rootMux.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		log.Debug().Msgf("TODO: /healthz actual check e.g db and other services")
		_, _ = w.Write([]byte("OK!"))
	})

	statsFilter := rest.NewStatsFilter()
	restContainer.Filter(statsFilter.Filter)
	rootMux.HandleFunc(apiBasePath+"/statz", statsFilter.StatsHandler)
	rootMux.Handle("/", apiMux)

	var shuttingDown bool
	shutdownSignal := make(chan os.Signal)
	signal.Notify(shutdownSignal, syscall.SIGINT, syscall.SIGTERM)

	apiServer := http.Server{
		Addr:    ":" + httpListenPort,
		Handler: rootMux,
	}

	go func() {
		log.Info().Msgf("service is ready...")
		err := apiServer.ListenAndServe()
		if err != nil && (err != http.ErrServerClosed || !shuttingDown) {
			log.Fatal().Err(err).Msg("server stopped unexpectedly")
		}
	}()

	<-shutdownSignal
	shuttingDown = true

	shutdownCtx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	_ = apiServer.Shutdown(shutdownCtx)
	log.Info().Msgf("done...")
}
