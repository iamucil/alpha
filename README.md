# Alpha

Boiler plate for go-restful-api

Checkout this repo and update into your need.

```shell
git clone https://gitlab.com/iamucil/alpha.git
```

## How to change module name

This is how you change module name from this default module into yours
```shell
go mod edit -module {NEW_MODULE_NAME}
find . -type f -name '*.go' \
  -exec sed -i -e 's,{OLD_MODULE},{NEW_MODULE},g' {} \;
```

## Docker

This repository is working with go modules, and running well with docker. Just run `docker-compose up --build` and rest-service will be running in your machine.

## What you need?

1. Environment
   1. copy file `alpha-server/etc/secrets/config.env.example` to `alpha-server/etc/secrets/config.env` or anything, just make sure this env is loaded in docker container. Find `env_file` field di docker-compose.yaml
   2. Development using hot reload.
      1. Create file `docker-compose.override.yaml`  
   
       ```yaml
       version: '3'
    
       services:
    
         alpha-rest:
           build:
             context: .
             dockerfile: ./alpha-server/development.Dockerfile
         volumes:
          - "./:/workspace"
       ```
   3. run services with `make run` or `docker-compose up --build`

## Structure

```shell
.
├── alpha-server
│   ├── etc
│   │   └── secrets
│   └── var
│       ├── db
│       └── object-store
├── pkg
│   ├── alpha
│   │   └── logging
│   └── alphaserver
│       ├── app
│       └── rest
├── resources
│   └── swagger-ui
├── tmp
└── tools

```
- `alpha-server`
  - main service is loaded here
- `pkg`
  - Internal package used for this service to run.
- `pkg/alpha`
- `pkg/alphaserver`
  - main server
- `pkg/alphaserver/app`
  - app for server, load initial app from here, database, storage (minio, s3), others clients 
- `pkg/alphaserver/rest`
  - Handle rest service.
- `resources`
  - html, assets etc

## Running service 

Access running service in localhost:18080 if using docker or localhost:8080 if you did not using docker, or define your port by env.  

`/v1/healthz` >> health check?  
`/v1/statz` >> rest-service statz  