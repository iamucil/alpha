# for auto reload go-code and hot loading the builder
# be aware for memory consumptions
FROM cosmtrek/air AS loader

FROM golang:1.16

WORKDIR /workspace

COPY go.mod go.sum ./

ARG revisionID=unknown
ARG buildTimestamp=unknown

ENV CGO_ENABLED=0
# HTTP
EXPOSE 8080

# hot loading
COPY --from=loader /go/bin/air /go/bin/air

COPY ./alpha-server/etc/secrets/.air.toml /root/.air.toml

ENTRYPOINT ["air", "-c", "/root/.air.toml"]